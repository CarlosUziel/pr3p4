package pr3p4;

public class GenerateRandomVector {

    public static int[] generateRandomVector(int size) {
        
        int[] result = new int[size];
        for (int i = 0; i < result.length; i++) {
            result[i] = RandInt.randInt(-size*3, size*3);
        }
        return result;       
    }
}
