package pr3p4;

import java.awt.BorderLayout;
import static java.awt.BorderLayout.NORTH;
import static java.awt.BorderLayout.SOUTH;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ApplicationFrame extends JFrame {

    public ApplicationFrame() {
        this.setLocationRelativeTo(null);
        this.setTitle("Menores que un umbral");
        this.setLayout(new BorderLayout());
        this.setResizable(false);
        this.setSize(400, 200);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel firstVectorPanel = createFirstVectorPanel();
        this.getContentPane().add(firstVectorPanel, NORTH);
        this.getContentPane().add(createResultVectorPanel(firstVectorPanel), SOUTH);
        this.setVisible(true);
    }

    private JPanel createFirstVectorPanel() {
        JPanel firstVectorPanel = new FirstVectorPanel();
        return firstVectorPanel;
    }

    private JPanel createResultVectorPanel(JPanel firstVectorPanel) {
        JPanel resultVectorPanel = new ResultVectorPanel((FirstVectorPanel) firstVectorPanel);
        return resultVectorPanel;
    }
}