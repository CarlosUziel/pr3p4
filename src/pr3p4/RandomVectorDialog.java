package pr3p4;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.swing.JPanel;
import javax.swing.JTextField;

class RandomVectorDialog extends JDialog {

    private int[] resultVector;
    private int elemNumb;

    public RandomVectorDialog(JPanel jPanel) {
        this.setLocationRelativeTo(jPanel);
        this.setLayout(new FlowLayout());
        this.setTitle("GENERADOR ALEATORIO DE VECTOR DE ENTEROS");
        this.setModal(true);
        this.getContentPane().add(createElemNumbField());
        this.getContentPane().add(createGenerateButton());
        this.pack();
        this.setVisible(true);
    }

    public int[] getVector() {
        return resultVector;
    }

    private JTextField createElemNumbField() {
        JTextField numOfElemField = new JTextField(20);
        numOfElemField.setText("Inserta el número de elementos...");
        numOfElemField.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
            }

            @Override
            public void focusLost(FocusEvent e) {
                if(!numOfElemField.getText().equals("")) 
                    try{
                        elemNumb = Integer.parseInt(numOfElemField.getText());
                    }catch(NumberFormatException ex){
                        JOptionPane.showMessageDialog(RandomVectorDialog.this, "Caracteres introducidos no válidos.", "ERROR", ERROR_MESSAGE);
                    }
            }
        });
        return numOfElemField;
    }

    private JButton createGenerateButton() {
        JButton generateButton = new JButton("GENERAR");
        generateButton.addActionListener((ActionEvent e) -> {
            resultVector = GenerateRandomVector.generateRandomVector(elemNumb);            
            this.dispose();
        });
        return generateButton;
    }
}
