package pr3p4;

import java.awt.BorderLayout;
import static java.awt.BorderLayout.NORTH;
import static java.awt.BorderLayout.SOUTH;
import static java.awt.FlowLayout.CENTER;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FirstVectorPanel extends JPanel {

    private JTextField firstVectorField;
    private int[] vector;

    public FirstVectorPanel() {
        this.setLayout(new BorderLayout());
        this.add(createRandomVectorButton(), NORTH);
        this.add(createCustomVectorButton(), CENTER);
        this.add(createFirstVectorField(), SOUTH);
        this.setVisible(true);
    }

    private JButton createRandomVectorButton() {
        JButton randomVectorButton = new JButton("GENERAR VECTOR DE ENTEROS ALEATORIO");
        randomVectorButton.addActionListener((ActionEvent e) -> {
            vector = new RandomVectorDialog(this).getVector();
            if (!(vector == null || vector.length == 0)) setGeneratedVector();
        });

        return randomVectorButton;
    }

    private JButton createCustomVectorButton() {
        JButton CustomVectorButton = new JButton("GENERAR VECTOR DE ENTEROS PERSONALIZADO");
        CustomVectorButton.addActionListener((ActionEvent e) -> {
            vector = new CustomVectorDialog(this).getVector();
            if (!(vector == null || vector.length == 0)) setGeneratedVector();
        });

        return CustomVectorButton;
    }

    private JTextField createFirstVectorField() {
        JTextField firstVectorJTextField = new JTextField(20);
        firstVectorJTextField.setEditable(false);
        firstVectorJTextField.setHorizontalAlignment(JTextField.CENTER);
        firstVectorField = firstVectorJTextField;
        return firstVectorJTextField;
    }

    private void setGeneratedVector() {
        firstVectorField.setText(Arrays.toString(vector));
    }

    public int[] getVector() {
        return vector;
    }
}
