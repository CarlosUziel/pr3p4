package pr3p4;

import java.awt.BorderLayout;
import static java.awt.BorderLayout.NORTH;
import static java.awt.BorderLayout.SOUTH;
import static java.awt.FlowLayout.CENTER;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.swing.JPanel;
import javax.swing.JTextField;

class ResultVectorPanel extends JPanel {

    private int[] vector;
    private int value;
    private JTextField resultVectorField;
    private FirstVectorPanel firstVectorPanel;

    public ResultVectorPanel(FirstVectorPanel firstVectorPanel) {
        this.firstVectorPanel = firstVectorPanel;
        this.setLayout(new BorderLayout());
        this.add(createValueField(), NORTH);
        this.add(createCalculateButton(), CENTER);
        this.add(createResultVectorField(), SOUTH);
        this.setVisible(true);
    }

    private JButton createCalculateButton() {
        JButton calculateButton = new JButton("CALCULAR");
        calculateButton.addActionListener((ActionEvent e) -> {
            vector = firstVectorPanel.getVector();
            if (vector == null || vector.length == 0) {
                JOptionPane.showMessageDialog(this, "No se ha creado ningún vector", "ERROR", ERROR_MESSAGE);
                return;
            }
            if (!search(vector, value)) {
                JOptionPane.showMessageDialog(this, "El elemento insertado no está presente en el vector", "ERROR", ERROR_MESSAGE);
                return;
            }
            int[] pini = {0}, pfin = {0}, valuePos = {0};
            try {
                Divide.divide(vector, 0, vector.length - 1, value, valuePos, pini, pfin);
            } catch (Exception ex) {
            }
            vector = ExtractSmallers.extractSmallers(vector, valuePos[0]);
            setResultVector();
        });
        return calculateButton;
    }

    private JTextField createValueField() {
        JTextField valueField = new JTextField(20);
        valueField.setHorizontalAlignment(JTextField.CENTER);
        valueField.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
            }

            @Override
            public void focusLost(FocusEvent e) {
                try{
                value = Integer.parseInt(valueField.getText());
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(ResultVectorPanel.this, "Caracteres introducidos no válidos.", "ERROR", ERROR_MESSAGE);
                }
            }
        });
        return valueField;
    }

    private JTextField createResultVectorField() {
        JTextField resultVectorJTextField = new JTextField(20);
        resultVectorJTextField.setEditable(false);
        resultVectorJTextField.setHorizontalAlignment(JTextField.CENTER);
        resultVectorField = resultVectorJTextField;
        return resultVectorJTextField;
    }

    private void setResultVector() {
        resultVectorField.setText(Arrays.toString(vector));
    }

    private boolean search(int[] vector, int value) {
        for (int i = 0; i < vector.length; i++) {
            if(vector[i] == value) return true;
        }
        return false;
    }
}
