package pr3p4;

public class Divide {

    public static void divide(int[] v, int linf, int lsup, int value, int[] valuePos, int[] pini, int[] pfin) throws Exception {

        if (linf > lsup) {
            System.out.println("linf > lsup");
            throw new Exception();
        }

        if (linf == lsup) {
            valuePos[0] = linf;
            return;
        }
        
        Division.division(v, linf, lsup, pini, pfin);

        if (v[pfin[0] + 1] == value) {
            valuePos[0] = pfin[0] + 1;
            return;
        }
        if (v[pfin[0] + 1] < value) {
            divide(v, pini[0], lsup, value, valuePos, pini, pfin);
        } else {
            divide(v, linf, pfin[0], value, valuePos, pini, pfin);
        }
    }

}
