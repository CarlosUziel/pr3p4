package pr3p4;

public class Division {

    public static void division(int[] vec, int linf, int lsup, int[] pini, int[] pfin) {
        int piv, aux;
        piv = RandInt.randInt(linf, lsup);
        pini[0] = linf;
        pfin[0] = lsup;
        
        while (pini[0] <= pfin[0]) {
            while ((pini[0] <= lsup) && (vec[pini[0]] <= vec[piv])) {
                pini[0] += 1;
            }

            while ((pfin[0] >= linf) && (vec[pfin[0]] >= vec[piv])) {
                pfin[0] -= 1;
            }

            if (pini[0] < pfin[0]) {
                aux = vec[pini[0]];
                vec[pini[0]] = vec[pfin[0]];
                vec[pfin[0]] = aux;
                pini[0] += 1;
                pfin[0] -= 1;
            }

        }

        if (pini[0] < piv) {
            aux = vec[pini[0]];
            vec[pini[0]] = vec[piv];
            vec[piv] = aux;
            pini[0] += 1;
        } else {
            if (pfin[0] > piv) {
                aux = vec[piv];
                vec[piv] = vec[pfin[0]];
                vec[pfin[0]] = aux;
                pfin[0] -= 1;
            }
        }
    }
}
