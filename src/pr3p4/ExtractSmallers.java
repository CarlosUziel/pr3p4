package pr3p4;

import java.util.Arrays;

public class ExtractSmallers {

    public static int[] extractSmallers(int[] vector, int valuePos) {
        int[] resultVector = new int[valuePos];

        System.arraycopy(vector, 0, resultVector, 0, valuePos);
        Arrays.sort(resultVector);
        for (int i = 0; i < resultVector.length / 2; i++) {
            int temp = resultVector[i];
            resultVector[i] = resultVector[resultVector.length - 1 - i];
            resultVector[resultVector.length - 1 - i] = temp;
        }
        return resultVector;
    }
}
