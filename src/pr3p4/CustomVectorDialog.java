package pr3p4;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CustomVectorDialog extends JDialog {

    private int[] resultVector;
    private String customVector;

    public CustomVectorDialog(JPanel jPanel) {
        this.setLocationRelativeTo(jPanel);
        this.setTitle("GENERADOR PERSONALIZADO DE VECTOR DE ENTEROS");
        this.setLayout(new FlowLayout());
        this.setModal(true);
        this.getContentPane().add(createHelpButton());
        this.getContentPane().add(createCustomVectorField());
        this.getContentPane().add(createGenerateButton());
        this.pack();
        this.setVisible(true);
    }

    public int[] getVector() {
        return resultVector;
    }

    private JTextField createCustomVectorField() {
        JTextField customVectorField = new JTextField(20);
        customVectorField.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
            }

            @Override
            public void focusLost(FocusEvent e) {
                customVector = customVectorField.getText();
            }
        });
        return customVectorField;
    }

    private JButton createGenerateButton() {
        JButton generateButton = new JButton("GENERAR");
        generateButton.addActionListener((ActionEvent e) -> {
            resultVector = stringToVector();
            this.setVisible(false);
            this.dispose();
        });
        return generateButton;
    }

    private int[] stringToVector() {
        int prev = 0, current;
        ArrayList<Integer> result = new ArrayList<>();
        try {
            while ((current = customVector.indexOf(",", prev)) != -1) {
                result.add(Integer.parseInt(customVector.substring(prev, current)));
                prev = current + 1;
            }
            result.add(Integer.parseInt(customVector.substring(customVector.lastIndexOf(",") + 1, customVector.length())));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Caracteres introducidos no válidos.", "ERROR", ERROR_MESSAGE);
            return null;
        }
        return intFromInteger(result);
    }

    private int[] intFromInteger(ArrayList<Integer> v) {
        int[] r = new int[v.size()];
        int i = 0;
        for (Integer s : v) {
            r[i++] = s;
        }
        return r;
    }

    private JButton createHelpButton() {
        JButton helpButton = new JButton("AYUDA");
        helpButton.addActionListener((ActionEvent e) -> {
            JOptionPane.showMessageDialog(this, "Formato del vector: \"ele0,ele1,ele2\" (sin comillas)");
        });
        return helpButton;
    }
}
